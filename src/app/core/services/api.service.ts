import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  apiUrl = environment.baseUrl;
  constructor( private http: HttpClient) {}

  get(path: string): Observable<any> {
    return this.http.get(this.apiUrl + path);
  }
  post(path: string, data: object): Observable<any> {
    return this.http.post(this.apiUrl + path, data);
  }
  put(path: string, data: object): Observable<any> {
    return this.http.put(this.apiUrl + path, data);
  }
  delete(path: string): Observable<any> {
    return this.http.delete(this.apiUrl + path);
  }
}
