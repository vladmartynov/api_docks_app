import { Injectable } from '@angular/core';
import { OrderService } from './order.service';
import { Order } from '../shared/types/order';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  constructor() {}

  public isActivated = false;

  public activate(): void {
    this.isActivated = true;
  }
}
