import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { Order } from '../shared/types/order';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  path = 'orders';
  constructor(private api: ApiService) {}

  public getOrder(params: string): Observable<Order> {
    const query = `?${params}`;
    return this.api.get(this.path + query);
  }
  public postOrder(data: object): Observable<any> {
    return this.api.post(this.path, data);
  }
  public deleteOrder(params: string): Observable<any> {
    const query = `?${params}`;
    return this.api.delete(this.path + query);
  }
  public putOrder(params: string): Observable<any> {
    const query = `?${params}`;
    return this.api.delete(this.path + query);
  }
}
