export interface Order {
    symbol: string;
    side: string;
    orderId: number;
    type: string;
    quantity: number;
    timestamp: number;
    price?:	number;
    stopPrice?:	number;
    newOrderRespType?: string;
    status?: string;
    time?: number;
    updateTime?: number;
    isWorking?: boolean;
}
