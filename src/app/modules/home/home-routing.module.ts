import { GetComponent } from './pages/get/get.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostComponent } from './pages/post/post.component';
import {PutComponent} from './pages/put/put.component';
import {DeleteComponent} from './pages/delete/delete.component';

const routes: Routes = [
  {
    path: '',
    component: PostComponent,
    canActivate: [],
    data: { animation: 'MainComponent' },
  },
  {
    path: 'post',
    component: PostComponent,
    canActivate: [],
    data: { animation: 'MainComponent' },
  },
  {
    path: 'get',
    component: GetComponent,
    canActivate: [],
    data: { animation: 'MainComponent' },
  },
  {
    path: 'put',
    component: PutComponent,
    canActivate: [],
    data: { animation: 'MainComponent' },
  },
  {
    path: 'delete',
    component: DeleteComponent,
    canActivate: [],
    data: { animation: 'MainComponent' },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomepageRoutingModule {}
