import { MockDataHeaderTransform } from './../../core/pipes/MockDataHeaderTransform.pipe';
import { CommonModule } from '@angular/common';
import { CoreModule } from './../../core/core.module';
import { HeaderComponent } from './../../core/shared/header/header.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HomepageRoutingModule } from './home-routing.module';
import { GetComponent } from './pages/get/get.component';
import { PostComponent } from './pages/post/post.component';
import { TransformTextDirective } from 'src/app/core/directives/transformText.directive';
import { HomeService } from 'src/app/core/services/home.service';
import { PutComponent } from './pages/put/put.component';
import {DeleteComponent} from './pages/delete/delete.component';

@NgModule({
  declarations: [
    GetComponent,
    PostComponent,
    PutComponent,
    DeleteComponent,
    HeaderComponent,
    MockDataHeaderTransform,
    TransformTextDirective,
    PostComponent
  ],
  imports: [
      HomepageRoutingModule,
      CoreModule,
      FormsModule,
      CommonModule,
      ReactiveFormsModule
  ],
  entryComponents: [],
  providers: [HomeService]
})
export class HomeModule {}
