import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/core/shared/types/order';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrderService} from '../../../../core/services/order.service';


@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss'],
})
export class DeleteComponent implements OnInit {
  deleteForm: FormGroup;
  message: any;
  error: any;

  constructor(private service: OrderService,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.deleteForm = this.formBuilder.group({
      symbol: ['', [Validators.required]],
      orderId: ['', Validators.required],
      timestamp: ['', Validators.required],
    });
  }
  deleteOrder() {
    if (this.deleteForm.valid) {
      const  params = `symbol=${this.deleteForm.value.symbol}&timestamp=${this.deleteForm.value.timestamp}&orderId=${this.deleteForm.value.orderId}`;
      this.service.deleteOrder(params).subscribe(response => {
        this.error = false;
        this.message = response;
      }, error => {
        this.error = error.message;
      });
    }
  }
}
