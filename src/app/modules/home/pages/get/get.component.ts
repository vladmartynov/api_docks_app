import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/core/shared/types/order';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrderService} from '../../../../core/services/order.service';


@Component({
  selector: 'app-get',
  templateUrl: './get.component.html',
  styleUrls: ['./get.component.scss'],
})
export class GetComponent implements OnInit {
  message: Order;
  error: any;
  getForm: FormGroup;

  constructor(private service: OrderService,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.getForm = this.formBuilder.group({
      symbol: ['', [Validators.required]],
      orderId: [''],
      timestamp: ['', Validators.required],
    });
  }
  getOrderList() {
    if (this.getForm.valid) {
      let params = `symbol=${this.getForm.value.symbol}&timestamp=${this.getForm.value.timestamp}`;
      if (this.getForm.value.orderId) {
        params = `symbol=${this.getForm.value.symbol}&timestamp=${this.getForm.value.timestamp}&orderId=${this.getForm.value.orderId}`;
      }
      this.service.getOrder(params).subscribe(response => {
        this.error = false;
        this.message = response;
      }, error => {
        this.error = error.message;
      });
    }
  }
}
