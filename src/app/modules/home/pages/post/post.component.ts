import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/core/shared/types/order';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrderService} from '../../../../core/services/order.service';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  postForm: FormGroup;
  message: any;
  error: any;

  constructor(private service: OrderService,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.postForm = this.formBuilder.group({
      symbol: ['', [Validators.required]],
      side: ['', [Validators.required]],
      type: ['', [Validators.required]],
      quantity: ['', [Validators.required,]],
      price: [],
      stopPrice: [],
      newOrderRespType: [],
      timestamp: [Date.now(), Validators.required],
    });
  }
  postOrder(): void {
    if (this.postForm.valid) {
      this.service.postOrder(this.postForm.value).subscribe(responce => {
        this.error = false;
        this.message = responce;
      }, error => {
        this.error = error.message;
      });
    }
  }
}
