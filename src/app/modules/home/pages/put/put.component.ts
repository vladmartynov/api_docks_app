import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/core/shared/types/order';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrderService} from '../../../../core/services/order.service';


@Component({
  selector: 'app-put',
  templateUrl: './put.component.html',
  styleUrls: ['./put.component.scss'],
})
export class PutComponent implements OnInit {
  putForm: FormGroup;
  message: any;
  error: any;

  constructor(private service: OrderService,
              private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.putForm = this.formBuilder.group({
      symbol: ['', [Validators.required]],
      orderId: [''],
      timestamp: ['', Validators.required],
    });
  }
  putOrder() {
    if (this.putForm.valid) {
      let params = `symbol=${this.putForm.value.symbol}&timestamp=${this.putForm.value.timestamp}`;
      if (this.putForm.value.orderId) {
        params = `symbol=${this.putForm.value.symbol}&timestamp=${this.putForm.value.timestamp}&orderId=${this.putForm.value.orderId}`;
      }
      this.service.putOrder(params).subscribe(response => {
        this.error = false;
        this.message = response;
      }, error => {
        this.error = error.message;
      });
    }
  }
}
